package de.torstenkohn.reactive.jfs.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Function;

import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

@SpringBootApplication
public class ReactiveJFSDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReactiveJFSDemoApplication.class, args);
    }
}

// #### Controllers ####
// using HTTPie in the terminal, see also https://httpie.org/
@RestController
@RequestMapping("/api/pizzas")
class PizzaRestController {

    private final PizzaService service;

    @Autowired
    PizzaRestController(PizzaService service) {
        this.service = service;
    }

    // HTTPie:
    // http GET :8080/api/pizzas
    @GetMapping
    public Flux<Pizza> all() {
        return this.service.all();
    }

    // HTTPie:
    // http GET :8080/api/pizzas/ID
    @GetMapping("/{pizzaId}")
    public Mono<ResponseEntity<Pizza>> byId(@PathVariable String pizzaId) {
        return this.service.byId(pizzaId)
                .map(pizza -> ok()
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .body(pizza))
                .defaultIfEmpty(notFound().build());
    }

    // HTTPie:
    // http POST :8080/api/pizzas name=Test
    @PostMapping
    public Mono<Pizza> add(@RequestBody Pizza pizza) {
        return this.service.save(new Pizza(UUID.randomUUID().toString(), pizza.getName()));
    }

    // HTTPie:
    // http DELETE :8080/api/pizzas/ID
    @DeleteMapping("/{pizzaId}")
    public Mono<Void> deleteById(@PathVariable String pizzaId) {
        return this.service.deleteById(pizzaId);
    }

    @GetMapping(value = "/{pizzaId}/orders", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<PizzaOrder> pizzaOrders(@PathVariable String pizzaId) {
        return this.service.pizzaOrders(pizzaId);
    }

    @GetMapping(value = "/debug")
    public Flux<Pizza> debug() {
        return this.service.debug();
    }

}


// #### Services ####
@Service
class PizzaService {
    private final PizzaRepository repository;

    @Autowired
    PizzaService(PizzaRepository repository) {
        this.repository = repository;
    }

    public Flux<Pizza> all() {
        return repository.findAll();
    }

    public Mono<Pizza> byId(final String pizzaId) {
        return repository.findById(pizzaId);
    }

    public Mono<Pizza> save(Pizza pizza) {
        return this.repository.save(pizza);
    }

    public Mono<Void> deleteById(String pizzaId) {
        return this.repository.deleteById(pizzaId);
    }

    public Flux<PizzaOrder> pizzaOrders(String pizzaId) {
        return Flux.<PizzaOrder>generate(sink -> sink.next(new PizzaOrder(pizzaId, new Date())))
                .delayElements(Duration.ofMillis(5000));
    }

    public Flux<Pizza> debug() {
        //Hooks.onOperatorDebug();
        return repository.findAll()
                .concatWith(Mono.error(new Throwable("Pizza is out!")));
                //.checkpoint("Pizza checkpoint", true);
    }
}

// #### Repositories ####
interface PizzaRepository extends ReactiveCrudRepository<Pizza, String> {

}


// #### Models ####
@Document
class Pizza {
    @Id
    private String id;
    private String name;

    public Pizza() {
    }

    public Pizza(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pizza pizza = (Pizza) o;
        return Objects.equals(id, pizza.id) &&
                Objects.equals(name, pizza.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "Pizza{" + "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}

class PizzaOrder {
    private String pizzaId;
    private Date dateOrdered;

    public PizzaOrder() {
    }

    public PizzaOrder(String pizzaId, Date dateOrdered) {
        this.pizzaId = pizzaId;
        this.dateOrdered = dateOrdered;
    }

    public String getPizzaId() {
        return pizzaId;
    }

    public Date getDateOrdered() {
        return dateOrdered;
    }

    @Override
    public String toString() {
        return "PizzaOrder{" + "pizzaId='" + pizzaId + '\'' +
                ", dateOrdered=" + dateOrdered +
                '}';
    }
}


// #### Other stuff ####

/**
 * The runner will initial add items to the database during the startup of the application.
 * It is only used for the demo
 */
@Component
class UserInitCommandLineRunner implements CommandLineRunner {

    private final PizzaRepository repository;

    @Autowired
    UserInitCommandLineRunner(PizzaRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run(String... args) {
        final Flux<String> pizzaNames = Flux.just(
                "Margherita", "Funghi", "Regina", "Tonno",
                "Quattro Formaggi", "Vegetariana", "Vulcano");

        this.repository
                .deleteAll() // is not needed because of usage of the embedded MongoDB
                .thenMany(
                        pizzaNames
                                .map(create())
                                .flatMap(this.repository::save))
                .subscribe(null, null,
                        () -> this.repository
                                .findAll()
                                .subscribe(System.out::println));
    }

    private Function<String, Pizza> create() {
        return name -> {
            final String pizzaId = UUID.randomUUID().toString();
            return new Pizza(pizzaId, name);
        };
    }
}



