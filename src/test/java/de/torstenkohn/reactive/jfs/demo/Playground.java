package de.torstenkohn.reactive.jfs.demo;

import org.junit.Test;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Hooks;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import reactor.util.function.Tuples;

import java.time.Duration;
import java.util.Arrays;
import java.util.function.Supplier;

import static org.assertj.core.api.Assertions.assertThat;

public class Playground {

    @Test
    public void createMonoAndSubscribe() {
        // Not a real thing, only to show something
        Mono.just("Hello World")
                .doOnNext(item -> System.out.println("Mono - " + item))
                .map(value -> value.split("(?!^)"))
                // now, we have an array of the characters
                .flatMapMany(Flux::fromArray)
                .doOnNext(item -> System.out.println("Flux - " + item))
                .filter(character -> !"L".equalsIgnoreCase(character))
                .doOnNext(item -> System.out.println("after filter - " + item))
                .collectList()
                .map(list -> String.join("", list))
                .subscribe(System.out::println);
    }

    @Test
    public void testCreateMono() {
        Mono<String> result = Mono.just("Hello World")
                .map(value -> value.split("(?!^)"))
                .flatMapMany(Flux::fromArray)
                .filter(character -> !"L".equalsIgnoreCase(character))
                .collectList()
                .map(list -> String.join("", list));

        StepVerifier.create(result)
                .expectSubscription()
                .expectNext("Heo Word")
                .verifyComplete();
    }

    @Test
    public void testCreateFlux() {
        Flux<String> programmingLangauges =
                Flux.just("Perl", "Java", "Scala", "Assembler",
                        "Clojure", "C", "JavaScript", "Kotlin");

        StepVerifier.create(programmingLangauges)
                .expectSubscription()
                // using own assertions to test
                .assertNext(item -> assertThat(item).isEqualTo("Perl"))
                .thenRequest(3)
                .expectNext("Java", "Scala", "Assembler")
                // using own assertions to test, different way
                .consumeNextWith(item -> assertThat(item).isEqualTo("Clojure"))
                .expectNext("C")
                .thenRequest(2)
                .expectNextCount(2)
                .verifyComplete();
    }

    @Test
    public void testFluxZipWith() {
        Flux<String> characters = Flux
                .just("A", "B", "C", "D");

        Flux<Number> numbers = Flux
                .fromIterable(
                        Arrays.asList(1, 2, 3));

        StepVerifier.create(characters.zipWith(numbers))
                .expectSubscription()
                .expectNext(Tuples.of("A", 1))
                .expectNext(Tuples.of("B", 2))
                .expectNextCount(1)
                .verifyComplete();
    }

    @Test
    public void testError() {
        Flux<String> fluxWithError = Flux.just("Hello", "World")
                .concatWith(Mono.error(new IllegalArgumentException("How about NO!")));
        StepVerifier.create(fluxWithError)
                .expectSubscription()
                .expectNext("Hello", "World")
                .expectErrorMessage("How about NO!")
                .verify();
    }

    @Test
    public void testInterval() {
        Flux<Long> elementsWithInterval =
                Flux.interval(Duration.ofSeconds(1))
                        .take(10);

        // will take time :‑(
        StepVerifier.create(elementsWithInterval)
                .expectNextCount(10)
                .expectComplete()
                .verify();
    }

    @Test
    public void testIntervalTimeTravel() {
        Supplier<Flux<Long>> elementsWithInterval = () -> Flux.interval(Duration.ofSeconds(1)).take(10);

        StepVerifier.withVirtualTime(elementsWithInterval)
                .expectSubscription()
                .thenAwait(Duration.ofSeconds(10))
                .expectNextCount(10)
                .expectComplete()
                .verify();
    }

    @Test
    public void debuggingHooks() {
        Hooks.onOperatorDebug();
        Mono.just("Hello World")
                .map(value -> value.split("(?!^)"))
                .flatMapMany(Flux::fromArray)
                .filter(character -> !"L".equalsIgnoreCase(character))
                .concatWith(Mono.error(new IllegalArgumentException("Error")))
                .collectList()
                .map(list -> String.join("", list))
                .subscribe(System.out::println);
        Hooks.resetOnEachOperator();
    }

    @Test
    public void debuggingCheckpoints() {
        Mono.just("Hello World")
                .map(value -> value.split("(?!^)"))
                .checkpoint("01 ")
                .flatMapMany(Flux::fromArray)
                //.concatWith(Mono.error(new IllegalArgumentException("too many")))
                .checkpoint("02 ")
                .filter(character -> !"L".equalsIgnoreCase(character))
                .checkpoint("03 ")
                //.concatWith(Mono.error(new IllegalArgumentException("Error")))
                .checkpoint("04 ", true)
                .collectList()
                .map(list -> String.join("", list))
                .checkpoint("05 ")
                .subscribe(System.out::println);
    }

    @Test
    public void ownSubscriber() {
        Flux.just("Dies ist ein Test mit ein paar Zeichen".split("(?!^)"))
                //.doOnNext(System.out::println)
                .subscribe(new Subscriber<String>() {
                    private Subscription subscription;
                    private int amount;
                    private String cache = "";

                    @Override
                    public void onSubscribe(Subscription subscription) {
                        this.subscription = subscription;
                        this.subscription.request(3);
                    }

                    @Override
                    public void onNext(String s) {
                        this.cache = this.cache.concat(s);
                        if (amount % 3 == 0) {
                            this.subscription.request(3);
                            this.cache = "";
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
