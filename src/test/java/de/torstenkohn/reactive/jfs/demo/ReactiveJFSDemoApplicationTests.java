package de.torstenkohn.reactive.jfs.demo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.FluxExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Hooks;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import reactor.test.StepVerifierOptions;

import java.time.Duration;
import java.util.function.Supplier;

@RunWith(SpringRunner.class)
@WebFluxTest(controllers = {PizzaRestController.class, PizzaService.class})
@AutoConfigureWebTestClient(timeout = "60000")
public class ReactiveJFSDemoApplicationTests {

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private PizzaService service;

    @MockBean
    private PizzaRepository repository;

    private Pizza pizza;

    @Before
    public void setUp() throws Exception {
        this.pizza = new Pizza("db7cdccf-3f79-4d6c-a94c-693841d006a1", "Test Pizza");
        Mockito.when(this.repository.findAll()).thenReturn(Flux.just(this.pizza));
        Mockito.when(this.repository.findById(this.pizza.getId())).thenReturn(Mono.just(this.pizza));
    }

    @Test
    public void testFindAll() {
        StepVerifier.create(
                webTestClient
                        .get()
                        .uri("/api/pizzas")
                        .exchange()
                        .expectStatus().isOk()
                        .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                        .returnResult(Pizza.class)
                        .getResponseBody()
                        .take(1))
                .expectNext(this.pizza)
                .verifyComplete();
    }

    @Test
    public void testFindAllJSONPath() {
        webTestClient
                .get()
                .uri("/api/pizzas/{id}", pizza.getId())
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody()
                .jsonPath("$").exists()
                .jsonPath("$.id").isEqualTo(pizza.getId())
                .jsonPath("$.name").isEqualTo(pizza.getName())
                .jsonPath("$.BLUB").doesNotHaveJsonPath();
    }

    @Test
    public void testEventsTake5VirtualTime() {
        FluxExchangeResult<PizzaOrder> exchangeResult = webTestClient
                .get()
                .uri("/api/pizzas/{pizzaId}/orders", pizza.getId())
                .accept(MediaType.parseMediaType("text/event-stream;charset=UTF-8"))
                .exchange()
                .expectStatus().isOk()
                .expectHeader()
                .contentType(MediaType.parseMediaType("text/event-stream;charset=UTF-8"))
                .returnResult(PizzaOrder.class);

        StepVerifier.withVirtualTime(() -> exchangeResult.getResponseBody())
                .thenAwait(Duration.ofSeconds(25))
                .expectNextCount(1)
                .thenCancel()
                .verify();
    }


    @Test
    public void testEventsTake5() {
        StepVerifier.create(
                webTestClient
                        .get()
                        .uri("/api/pizzas/{pizzaId}/orders", pizza.getId())
                        .exchange()
                        .expectStatus().isOk()
                        .expectHeader()
                        .contentType(MediaType.parseMediaType("text/event-stream;charset=UTF-8"))
                        .returnResult(PizzaOrder.class)
                        .getResponseBody()
                        .take(5))
                .expectNextCount(5)
                .verifyComplete();
    }

    @Test
    public void testServiceEventsTake5VirtualTime() {

        Supplier<Flux<PizzaOrder>> pizzaOrdersOf5 = () -> this.service.pizzaOrders(pizza.getId()).take(5);

        StepVerifier.withVirtualTime(pizzaOrdersOf5)
                .thenAwait(Duration.ofSeconds(5))
                .expectNextCount(1)
                .thenAwait(Duration.ofSeconds(20))
                .expectNextCount(4)
                .expectComplete()
                .verify();
    }
}
